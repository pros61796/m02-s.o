## Autor:          PE2_UF2_1HISM_KHARMICH FOURKATI_SAID.md
## Date:           13/04/21
## Description:    Prova M02 - UF02 Linux 1  
## Entrega Prova escrita:   

1. **( 1 punt ) Copia aquest fitxer en el teu repositori remot ( GitLab, GitHub ), a la carpeta /m-02/uf2/prova-escrita/**  
**Escriu aquí el link.**   

2. **(1 punt ) Escriu un script que ens dongui informació sobre la caducitat i validesa dels usuaris del Sistema.**  
#!/bin/bash
sudo cat /etc/shadow
if [$? -eq 0]; then 
	echo "l'ordre s'ha realitzat correctament"
	exit0
else
	echo "Comanda fallida"
	exit 1	
**Si l'ordre retorna un estat de sortida 0, informeu que "l'ordre s'ha realitzat correctament" i sortiu amb un estat de sortida 0.**  
**Si l'ordre retorna un estat de sortida diferent de zero, informeu "Comanda fallida" i sortiu amb un estat de sortida 1.** 

3. **( 1 punt ) Quin cron farem servir per tal que l'script anterior es faci a les 23:45 cada diumenge?** 
45  23  *  *  0

4. **(1 punt ) Feu un script que comprovi si existeix el fitxer "/tmp/prova". Si no existeix el crea. Feu que informi a l'usuari del que ha passat.**
#!/bin/bash
if [ -f /tmp/prova ];
then
echo "Si que existe "
else
echo "No existe"
fi

5. **(1 punt ) Que estem fent en la següent comanda? grep -w 8 fitxer**
El que fa aquesta comanda es forçar a buscar la linia 8 del fitxer en el que estem.

6. **(1 punt ) Com comprovem que la anterior ordre s'hagi executat amb èxit?.**
some_command
if [ $? -eq 0 ]; then
	echo OK
else
	echo FAIL
fi	

7. **(1 punt ) Crea un fitxer .txt de nom la data i hores actuals a /tmp**
touch fitxer.txt /home/Downloads/tmp 

8. **(1 punt ) Fer un script que rep tres arguments i valida que siguin exactament 3.**
#!/bin/bash
echo "Primer $0"
echo "Segon $1"
echo "Tercer $2"

9. **(1 punt ) Que fa la següent comanda? cat file1.txt file2.txt file3.txt | sort > file4.txt**
El que fa es crear el fitxer 1,2,3 i els ordena tots en el fitxer4.

10. **(1 punt ) Si tenim en un fitxer el següent contingut:**

```
one	two	three	four	five
alpha	beta	gamma	delta	epsilon
gener   febrer  març    abril
```

**Quina ordre hem d'executar per obtenir**
sort -k 2
```
two
beta
febrer
```



