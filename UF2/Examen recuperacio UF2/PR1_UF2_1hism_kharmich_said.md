## Autor:          PR1_UF2_1hism_kharmich_said.md
## Date:           08/06/21
## Description:    Prova Recuperació M02 - UF02 Linux 1  
#### 1. Descarrega't el fitxer markdown annexat a la tasca.
#### 2. PLEASE !! Renombra el fitxer amb el següent format: PR1_UF2_Grup_Cognom_alumne.md
#### 3. Un cop hagis acabat, annexa el fitxer markdown al moddle i pujar-ho al GIT. Totes dues coses.
#### 4. Els scripts que has creat, copia'ls al markdown actual i deixa'n una còpia al GIT.

--------

1. **[1 punt] Feu un script que creï un fitxer a /tmp que porti per nom el vostre nom i la data i hores actuals en el format nom_cognom_08062021120025.txt on tenimel nom, cognom, la data (08 06 2021) i l'hora (12 00 25) actuals amb segons inclosos. Anomeneu aquest script pregunta1.sh i poseu-hi una capçalera complerta. L'script es trobarà al directori /opt**
#!/bin/bash
# Name:Said Kharmich Fourkati
# Data: 08/06/2021
# Usage: bash pregunta1.sh
# File Location: /opt
# Description:

DATE=$(date +"%Y%m%d")

touch /tmp said_kharmich_${DATE}.txt

if [ $? -eq 0 ];
then
    echo "S'ha creat el fitxer"
else
    echo "No s'ha pogut crear el fitxer"
    exit 1
fi

echo said_kharmich > /tmp said_kharmich${DATE}.txt

if [ $? -eq 0 ];
then
    echo "S'ha afegit el said kharmich"
else
    echo "No s'ha pogut afegit el said kharmich"
    exit 1
fi

echo $(date "+%Y-&m-&d-&H:&M:%s") > /tmp said_kharmich${DATE}.txt

if [ $? -eq 0 ];
then
    echo "S'ha afegit la data"
else
    echo "No s'ha pogut afegit la data"
    exit 1
fi

2. **[1 punt] L'script ha de retornar codi d'error 0 (correcte) si s'ha pogut crear el fitxer i 127 (incorrecte) si no ha pogut crear el fitxer perquè ja existeixen.**

3. **[1 punt] Quin cron farem servir per tal que l'script anterior es faci a les 23:45 cada diumenge?**
45  23  *  *  0

4. **[1 punt] Quin cron farem servir per tal de realitzar el mateix que fa l'script de la pregunta 1 però sense fer ús de l'script? (és a dir, l'ordre directament s'executa al cron, no cridant a l'script)**

5. **[1 punt] Feu un script que comprovi si al directori /opt hi ha mes de dos fitxers. Si hi ha més de dos fitxers ha de retornar el codi de sortida 0 i si hi ha menys o igual a dos fitxers el codi de sortida 17. S'ha d'anomenar pregunta5.sh**
CONTADOR=$(ls /opt| wc -l)
If [ CONTADOR > 2 ]; then
echo "Sortida 0"
elif [ CONTADOR <= 2 ]; then
echo "Sortida 17"

6. **[2 punt] Farem un script per renombrar el nom dels fitxers segons l'extensió.**
**Preguntar a l’usuari quina extensió de fitxer volem considerar. ( per exemple .gif )**
**Preguntar a l’usuari quin prefix vol afegir als noms del fitxer per identificar-los.**
**Per defecte, el prefix ha de ser la data actual en format DDMMAAAA**
**Si l'usuari simplement prem Retorn, s'utilitzarà la data actual**
**En cas contrari, s’utilitzarà com a prefix el que hagi introduït l’usuari.**
**per acabar, heu de canviar el nom del fitxer.**

7.
**[0,5 punt] Amb quina ordre podrem extreure els camps compresos entre el 2 i el 5 d'un fitxer data.csv separat per tabuladors?**
cut -f 2 fitxer.txt
cut -f 5 fitxer.txt
**[0,5 punt] Quina ordre ens mourà al nostre directori personal en un Linux?**
-Per accedir a un directori personal s'utiliza la comanda cd i el directori en el que vols entrar(cd Documents).

**[0,5 punt] Quins permisos tindrà l'usuari mmarti sobre el fitxer: 16778304 16 -rw-r-x-w- 1 amarcilla inf 13331 9 jul 08:57 fitxer**
El permisos que tendra son que el usuari pot escriure i lleigir el contingut, el grup al que pertany el usuari pot nomes lleigir la resta d'usuaris nomes te permis d'execucio.

**[0,5 punt] Quina utilitat té el interrogant en la seguent comanda? ls pr?va**
S'utiliza per representar un caracter i el que mes s'assemblin sortiran a la sortida. exemple (prova,preva,prava).
**[0,5 punt] Quina ordre ens mourà al directori /tmp si ens trobem a /home/usuari?**
Primer haurem de tornar al directori principal amb la comanda cd .. i despres entrarem al directori tmp posant cd /tmp.

**[0,5 punt] Per a que serveix la comanda grep?.**
L'ordre grep  és un comandament que es fa servir des de la consola o terminal per a cercar un text en sistemes operatius basats en Unix. exemple:

    grep fruita *

i el programa mostrarà totes les línies de text que contenen la paraula fruita. 
