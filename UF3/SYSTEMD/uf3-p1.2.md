## Autor:          PE2_UF3_1HISM_Kharmich_Said.md
## Date:           28/05/21
## Description:    Prova M03 - UF03 SystemD, Journal, Firewall, Quotes
## Entrega Prova escrita UF3

En finalitzar l'exercici, el fitxer l'heu d'annexar al Moddle, **anomena'l amb el següent format: PE2_UF3_Grup_Cognom_nom.md**
També, recorda pujar-ho al GIT, a la carpeta UF3/prova-escrita/

## Enunciat

Estem encarregats d'administrar el sistema operatiu linux d'un servidor a la nostra empresa. Se'ns demana que fem diverses coses per tal d'obtenir un servidor amb les funcionalitats que es requereixen:

- Cal un servidor web.
- Caldrà establir un tallafocs per tal que no ens accedeixin a ports que no siguin públics.
- Caldrà un accés de ssh per a la gestió remota d'aquest servidor, però volen que només sigui accessible en horari d'oficina (tot i que la màquina estarà en funcionament tot el dia)

1. [2 punts] **Cal que aquesta màquina vostra tingui accés per ssh i que estigui en hora. Per això comprovarem els serveis:**

    - [0,25 punts] Ordre per activar el servei de ssh en iniciar el sistema operatiu: **sudo systemctl enable ssh**
    - [0,25 punts] Ordre per arrencar ara el servei de ssh: **sudo systemctl start ssh**
    - [0,25 punts] Odre i sortida per veure l'estat del servei ssh: **sudo systemctl status ssh**
sudo systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; disabled; vendor preset: enabled)
   Active: active (running) since Fri 2021-05-28 10:09:28 CEST; 1s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
  Process: 4712 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
 Main PID: 4713 (sshd)
    Tasks: 1 (limit: 4915)
   Memory: 2.0M
   CGroup: /system.slice/ssh.service
           └─4713 /usr/sbin/sshd -D

May 28 10:09:28 a09 systemd[1]: Starting OpenBSD Secure Shell server...
May 28 10:09:28 a09 sshd[4713]: Server listening on 0.0.0.0 port 22.
May 28 10:09:28 a09 sshd[4713]: Server listening on :: port 22.
May 28 10:09:28 a09 systemd[1]: Started OpenBSD Secure Shell server.

    - [0,25 punts] Ordre per activar el servei d'hora en iniciar el sistema operatiu: **sudo systemctl active ntpd**
    - [0,25 punts] Ordre per arrencar ara el servei d'hora: **sudo systemctl start ntpd**
    - [0,25 punts] Ordre i sortida per veure l'estat del servei d'hora: **sudo systemctl status ntp.service**

[isard@f32-isard ~]$ sudo systemctl status ntpd.service
● ntpd.service - Network Time Service
     Loaded: loaded (/usr/lib/systemd/system/ntpd.service; enabled; vendor pres>
     Active: inactive (dead)
...skipping...
● ntpd.service - Network Time Service
     Loaded: loaded (/usr/lib/systemd/system/ntpd.service; enabled; vendor pres>
     Active: inactive (dead)

    - [0,5 punts] Ordre per veure tots els seveis que es troben arrencats actualment: **sudo systemctl | grep running**

2. [2 punts] **Instal.leu ara el servidor web nginx. Aquest servidor ens permetrà tenir un servei en producció:**

    - [0,50 punts] Ordre per instal.lar el servidor web nginx al vostre sistema operatiu: **sudo dnf install ngix**
    - [0,50 punts] Ordre per activar el servei nginx en iniciar el sistema operatiu: **sudo systectl enable nginx**
    - [0,50 punts] Ordre per arrencar ara el servei nginx: **sudo systemctl start nginx**
    - [0,50 punts] Ordre i sortida per a veure els últims 10 missatges de log del servei nginx: sudo journalctl -n nginx.service 
[isard@f32-isard ~]$ sudo journalctl -n nginx.service
-- Logs begin at Mon 2020-10-19 01:43:31 CEST, end at Fri 2021-05-28 10:53:39 CEST. --
May 28 10:53:35 f32-isard sudo[34552]: pam_unix(sudo:session): session opened for user root by (uid=0)
May 28 10:53:35 f32-isard audit[34552]: USER_END pid=34552 uid=0 auid=1000 ses=1 subj=unconfined_u:unconfined_r:unc>
May 28 10:53:35 f32-isard audit[34552]: CRED_DISP pid=34552 uid=0 auid=1000 ses=1 subj=unconfined_u:unconfined_r:un>
May 28 10:53:35 f32-isard sudo[34552]: pam_unix(sudo:session): session closed for user root
May 28 10:53:39 f32-isard audit[34560]: USER_ACCT pid=34560 uid=1000 auid=1000 ses=1 subj=unconfined_u:unconfined_r>
May 28 10:53:39 f32-isard audit[34560]: USER_CMD pid=34560 uid=1000 auid=1000 ses=1 subj=unconfined_u:unconfined_r:>
May 28 10:53:39 f32-isard sudo[34560]:    isard : TTY=pts/0 ; PWD=/home/isard ; USER=root ; COMMAND=/usr/bin/journa>
May 28 10:53:39 f32-isard audit[34560]: CRED_REFR pid=34560 uid=0 auid=1000 ses=1 subj=unconfined_u:unconfined_r:un>
May 28 10:53:39 f32-isard audit[34560]: USER_START pid=34560 uid=0 auid=1000 ses=1 subj=unconfined_u:unconfined_r:u>
May 28 10:53:39 f32-isard sudo[34560]: pam_unix(sudo:session): session opened for user root by (uid=0)
lines 1-11/11 (END)

3. [2 punts] **Ara cal assegurar el servei ssh i el web com a els únics als que permetem accés:**

   - [0,25 punts] Ordre per activar el servei de tallafocs en iniciar el sistema operatiu: **sudo systemctl enable firewall**

   - [0,25 punts] Ordre per arrencar ara el servei de tallafocs: **sudo systemctl start firewall**
   - [0,25 punts] Ordre i sortida on es vegi els ports que el tallafocs està filtrant actualment i la zona activa: **sudo firewall-cmd --list-all  -  firwall-cmd --zone public --add port=zz/top**
[isard@f32-isard ~]$ sudo firewall-cmd --list-all - firwall-cmd --zone public --add port=zz/top
usage: see firewall-cmd man page
firewall-cmd: error: ambiguous option: --add could match --add-lockdown-whitelist-command, --add-lockdown-whitelist-context, --add-lockdown-whitelist-uid, --add-lockdown-whitelist-user, --add-interface, --add-source, --add-rich-rule, --add-service, --add-port, --add-protocol, --add-source-port, --add-masquerade, --add-icmp-block, --add-icmp-block-inversion, --add-forward-port, --add-entry, --add-entries-from-file, --add-destination, --add-module, --add-helper, --add-include, --add-passthrough, --add-chain, --add-rule

   - [0,5 punts] Ordre(s) per tal d'aconseguir que només siguin accessibles els ports 22 i 80 del servidor a partir de la informació mostrada en l'ordre anterior: **firewall-cmd --zone=public --add-part= 22/tcp**
**firewall-cmd --zone=public --add-part= 80/tcp** 

   - [0,5 punts] Ordre i sortida on es vegi els ports que el tallafocs està filtrant actualment i la zona activa:
   - [0,25 punts] Feu que aquesta configuració sigui permanent. Indiqueu la/les ordre(s) per a fer-ho:
**firewall-cmd --zone=public --add-port= 80/tcp --permanent**
**firewall-cmd --zone=public --add-port= 22/tcp --permanent**

4. [2 punts] **Feu ara dos scripts que comprovin l'estat del servei nginx. Un l'haurà d'arrencar si no ho està i l'altre l'haurà d'aturar si està arrencat**

    - [0,5 punts] Script que comprova si està arrencat i l'atura
#!/bin/bash
mysql=`ps awx | grep 'nginx' |grep -v grep|wc -l`
if [ $mysql == 0 ]; then
    service nginx desable
    echo "Nginx estaba activo y el cron lo desactivo."
fi
    - [0,5 punts] Script que comprova si està aturat i l'arrenca
#!/bin/bash
mysql=`ps awx | grep 'nginx' |grep -v grep|wc -l`
if [ $mysql == 0 ]; then
    service nginx restart
    echo "Nginx estaba apagado y el cron lo reactivo."
fi
    - [0,5 punts] Què hauríem de canviar al nostre script per tal que enviés un missatge de prioritat 'info' al journal amb informació que ens digui si l'hem arrencat o si l'hem parat?
    - [0,5 punts] Com podem veure els missatges de prioritat 'info' del log?
   **sudo journalctl -p info -b**

5. [2 punts]  **Una mica sobre quotes:** 
   - [0,25 punts] Per a que serveixen les quotes de disc?  LesQuotes de disc serveix per a la restricció de l'espai disponible per a l'escriptura i l'emmagatzematge en un disc dur físic o virtual. 

   - [0,50 punts] creem un disc virtual de 80Mb anomenat examen a la carpeta /home/
**sudo dd if=/dev/zero of=home/users/inf/hism1/a191873sk/examen bs=80k count=1000**
   - [0,25 punts] Al disc virtual que acabem de crear li donem format ext4
**sudo mkfs.ext4 /opt/disc**
   - [0,50 punts] Montem l'arxiu examen a la carpeta /mnt/usuari
**sudo mount -t examen /opt/disc /mnt**
   - [0,25 punts] Quin fitxer hem de modificar, per tal de que es munti el disc automàticament en reiniciar el sistema. 
   - [0,25 punts] Que és un inode?
És una estructura de dades pròpia dels sistemes d'arxius tradicionalment emprats en els sistemes operatius tipus UNIX com és el cas de Linux.




