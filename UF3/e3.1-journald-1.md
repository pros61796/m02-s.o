# E3. Exercici 2. JournalD

## Introducció

## Continguts

Amb els fiters i directoris de la carpeta exercici responeu les segûents preguntes:

Indiqueu en cada pregunta l'ordre i també el resultat obtingut (podeu copiar del terminal)

## Entrega

1. **Analitzeu els logs de l'última arrencada del vostre sistema operatiu i indiqueu:**
   - Ordre per mostrar el log: guest@a08:~$ sudo journalctl
-- Logs begin at Thu 2021-04-29 16:07:06 CEST, end at Fri 2021-04-30 10:29:15 CEST. --
abr 29 16:07:06 a08 kernel: Linux version 4.19.0-16-amd64 (debian-kernel@lists.debian.org) (g
abr 29 16:07:06 a08 kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-4.19.0-16-amd64 root=UUID=
abr 29 16:07:06 a08 kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point regi
abr 29 16:07:06 a08 kernel: x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'
abr 29 16:07:06 a08 kernel: x86/fpu: Enabled xstate features 0x3, context size is 576 bytes, 
abr 29 16:07:06 a08 kernel: BIOS-provided physical RAM map:
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x0000000000000000-0x000000000009d7ff] usable
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x000000000009d800-0x000000000009ffff] reserved
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x00000000000e0000-0x00000000000fffff] reserved
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x0000000000100000-0x00000000c6f51fff] usable
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x00000000c6f52000-0x00000000c6f58fff] ACPI NVS
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x00000000c6f59000-0x00000000c7f86fff] usable
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x00000000c7f87000-0x00000000c84f8fff] reserved
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x00000000c84f9000-0x00000000d9f1efff] usable
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x00000000d9f1f000-0x00000000da132fff] reserved
abr 29 16:07:06 a08 kernel: BIOS-e820: [mem 0x00000000da133000-0x00000000da218fff] ACPI NVS

   - Ordre per veure només els errors amb prioritat d'error des de l'última arrencada: guest@a08:~$ sudo journalctl  -p err
-- Logs begin at Thu 2021-04-29 16:07:06 CEST, end at Fri 2021-04-30 10:30:33 CEST. --
abr 29 16:07:06 a08 kernel: [Firmware Bug]: TSC_DEADLINE disabled due to Errata; please updat
abr 29 16:07:16 a08 systemd[1]: Failed to start VirtualBox Linux kernel module.
abr 29 16:07:16 a08 kernel: r8169 0000:02:00.0: firmware: failed to load rtl_nic/rtl8168e-3.f
abr 29 16:07:16 a08 kernel: firmware_class: See https://wiki.debian.org/Firmware for informat
abr 29 16:07:53 a08 gnome-session-binary[992]: Unrecoverable failure in required component or
abr 29 16:41:38 a08 dhclient[3101]: Failed to get interface index: No such device
abr 29 16:41:38 a08 dhclient[3101]: 
abr 29 16:41:38 a08 dhclient[3101]: If you think you have received this message due to a bug 
abr 29 16:41:38 a08 dhclient[3101]: than a configuration issue please read the section on sub
abr 29 16:41:38 a08 dhclient[3101]: bugs on either our web page at www.isc.org or in the READ
abr 29 16:41:38 a08 dhclient[3101]: before submitting a bug.  These pages explain the proper
abr 29 16:41:38 a08 dhclient[3101]: process and the information we find helpful for debugging
abr 29 16:41:38 a08 dhclient[3101]: 

   - Ordre per veure només els errors amb prioritat d'emergència des de l'última arrencada: guest@a08:~$ sudo journalctl  -p emerg
-- Logs begin at Thu 2021-04-29 16:07:06 CEST, end at Fri 2021-04-30 10:36:28 CEST. --
-- No entries --

   - Ordre per veure només els errors amb prioritat d'alerta des de l'última arrencada:guest@a08:~$ sudo journalctl  -p alert
-- Logs begin at Thu 2021-04-29 16:07:06 CEST, end at Fri 2021-04-30 10:41:58 CEST. --
-- No entries --
2. **Com podem veure el log del nostre mini-servidor web que arrenquem amb el systemd?**
   - Ordre:guest@a08:~$ sudo Journalctl -u myweb.service
   - Sortida de l'ordre anterior:
abr 30 11:22:51 a08 sudo[8145]:    guest : TTY=pts/0 ; PWD=/home/guest ; USER=root ; COMMAND=/usr/bin/systemctl status myweb.service
abr 30 11:23:24 a08 sudo[8151]:    guest : TTY=pts/0 ; PWD=/home/guest ; USER=root ; COMMAND=/usr/bin/systemctl enable myweb.service
abr 30 11:25:03 a08 sudo[8197]:    guest : TTY=pts/0 ; PWD=/home/guest ; USER=root ; COMMAND=/usr/bin/systemctl enable myweb.service
abr 30 11:25:56 a08 sudo[8258]:    guest : TTY=pts/0 ; PWD=/home/guest ; USER=root ; COMMAND=/usr/bin/mv myweb.service /etc/systemd/system
abr 30 11:26:02 a08 sudo[8262]:    guest : TTY=pts/0 ; PWD=/home/guest ; USER=root ; COMMAND=/usr/bin/systemctl enable myweb.service
abr 30 11:26:48 a08 sudo[8340]:    guest : TTY=pts/0 ; PWD=/home/guest ; USER=root ; COMMAND=/usr/bin/systemctl status myweb.service
abr 30 11:27:13 a08 sudo[8344]:    guest : TTY=pts/0 ; PWD=/home/guest ; USER=root ; 

3. **Com podem veure els accessos que es fan al nostre servidor en temps real des del log de journal? **
   - Ordre:guest@a08:~$ sudo journalctl -f | grep myweb 

4. **Feu un cron que comprovi cada minut si el servei sshd està corrent o no al nostre sistema operatiu.**
   - Ordre i/o fitxers per a dur a terme el cron: 
	*/1 * * * * 
	systemctl is-active sshd
5. **Feu ara que aquest cron miri si el servei sshd està corrent i el pari i enviï un missatge d'error de nivell emergència al journal..**
   - Ordre i/o fitxers per a dur a terme el cron:
	#!/bin/bash
	systemctl is-active sshd
	id [ $? == 0 ]; then
	echo " FATAL ERROR" | systemctl - cat -p emerg
else
	echo "TOT CORRECTE" | systemctl - cat -p info 
fi
   - Proveu d'iniciar i aturar aquest servei i mostreu l'ordre amb la que podem monitoritzar si està o no funcionant (mostra missatges al journal)
