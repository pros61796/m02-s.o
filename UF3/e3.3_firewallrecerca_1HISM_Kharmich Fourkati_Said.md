# Autor:          e3.4_Firewall_1HISM_Kharmich Fourkati_Said.md
# Date:           04/05/21
# Description:    E3.4 Treball de recerca sobre els Firewalls

## 1. Qué és un tallafocs?. 
-Un tallafoc és la part d'un sistema informàtic o una xarxa informàtica que està dissenyada per bloquejar l'accés no autoritzat.


## 2. Funcions dels tallafocs?. 
 -La sevafunció és prevenir i protegir la nostra xarxa privada, d'intrusions o atacs d'altres xarxes, bloqueándole l'accés. Permet el trànsit entrant i sortint que hi ha entre xarxes o ordinadors d'una mateixa xarxa.

## 3. Qué és el Firewalld?. 
És una eina de gestió de tallafocs per a sistemes operatius Linux. Proporciona funcions de tallafocs a l'actuar com un front-end per al marc de netfilter de el nucli de Linux a través de la utilitat nftables userspace.

## 4. Qué és una zona en un firewall?. Quina és la zona d'un Firewall de més confiança?. i la de menys?
-Les zones són conjunts de regles predefinides per a diversos nivells de confiança que probablement faria servir en ubicacions o escenaris comuns.
Zona no fiable: una zona de seguretat amb un nivell de seguretat baix (nivell 5).

 
DMZ: una zona de seguretat amb un nivell de seguretat mitjà (nivell 50).

 
Zona de confiança: una zona de seguretat amb un nivell de seguretat alt (nivell 85).

 
Zona local: una zona de seguretat amb el nivell de seguretat més alt (nivell 100).


## 5. Que defineixen les regles en un Firewall?. 
Les regles de tallafocs defineixen quin tipus de tràfic d'Internet es permet o bloqueja. ... Una regla de tallafocs consta de serveis de tallafocs, que especifiquen el tipus de trànsit i els ports que aquest tipus de tràfic pot utilitzar.



## 6. Com verifiquem que el servei de firewall està en execució?
Es pot mirar amb la comanda sudo ufw status.

## 7. Com instal·lem i habilitem un tallafoc?
El primer que pots fer és comprovar si UFW aquesta actiu o no. Tot i que ja t'avanço que, tret que ho hagis modificat prèviament, en Ubuntu estarà deshabilitat.
sudo ufw status

sudo ufw enable -Per habilitar UFW

sudo ufw default deny -Aquesta comanda ha de denegar la entrada de conexions del exterior.

sudo ufw disable -Per desabilitar UFW.
## 8. Com explorem les zones disponibles d'un firewall?
Firewall-cmd --list-all.


## 9. Com sabem quina zona del Firewall està seleccionada com a predeterminada? 
Firewall-cmd --get-default-zone


## 10. Podem veure la configuració específica associada a la zona home?
Firewall-cmd --zone=home --list-all


## 11. Com sabem quines regles estan associades a la zona pública? 
Firewall-cmd --zone=public --list-services


## 12. Que és una xarxa?. Com fem per indicar les interficies d'una xarxa?. Com activem una interficie de xarxa?  

sudo firewall-cmd --state

sudo firewall-cmd --reload


## 13. Com indiquem que una regla sigui permanent? 

sudo firewall-cmd --zone=xxx --add-interface=yyy **--permanent**



## 14. Com podem saber els serveis disponibles del tallafocs?

sudo firewall-cmd --get-services



## 15. Com podem canviar una regla?

sudo firewall-cmd --zone=xxx **--add-service=regla x** --permanent
sudo firewall-cmd --reload


## 16. Si volem saber més informació de cadascun dels serveis del firewall. Com ho podem fer?.

sudo firewall-cmd --get-services


## 17. Quina es la manera mes senzilla de definir excepcions de tallafocs per a serveis ?

sudo firewall-cmd --zone=dmz --add-service=http --permanent


## 18. Com podem habilitar un servei per a una zona?. Com podem verificar que el servei es faci permanent (que el  servei encara estigui disponible després d'un reinici. )? 

sudo firewall-cmd --zone=**zona que vulguis** --add-service=xxx **--permanent**



## 19. Que és un port?. Com obrim un port per a una zona?. Com verifiquem que hagi anat bé?
Un port es una interfície a través de la cual es poden enviar i rebre els diferents tipùs de dades.
Obrir el port: sudo firewall-cmd --zone=xxx **--add-port=xxx/tcp --permanent**
Verificar el port: netstat -tulpen | grep "puerto que quieras"





## 20. Fes un dibuix on apareguins els següents elements: Linux, Kernel, Firewall, zones, Regles, serveis, terminal, xarxa, port, wifi, public, home, work, Internet. Annexa'l al markdown.
